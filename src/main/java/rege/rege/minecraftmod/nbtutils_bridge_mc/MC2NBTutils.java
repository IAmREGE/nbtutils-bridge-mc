package rege.rege.minecraftmod.nbtutils_bridge_mc;

import net.minecraft.nbt.NbtByte;
import net.minecraft.nbt.NbtByteArray;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtDouble;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtEnd;
import net.minecraft.nbt.NbtFloat;
import net.minecraft.nbt.NbtInt;
import net.minecraft.nbt.NbtIntArray;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtLong;
import net.minecraft.nbt.NbtLongArray;
import net.minecraft.nbt.NbtShort;
import net.minecraft.nbt.NbtString;
import net.minecraft.nbt.NbtType;

import rege.rege.nbtutils.NBTCompound;
import rege.rege.nbtutils.NBTList;
import rege.rege.nbtutils.NBTTag;
import rege.rege.nbtutils.NBTTagType;
import rege.rege.utf8chr.UTF8Sequence;

public class MC2NBTutils {
    private MC2NBTutils() {
        throw new UnsupportedOperationException();
    }

    public static NBTTag convertNBTTag(NbtElement orig) {
        final NbtType<?> TYPE = orig.getNbtType();
        if (TYPE == NbtEnd.TYPE) {
            return new NBTTag(NBTTagType.TAG_End);
        }
        if (TYPE == NbtByte.TYPE) {
            return new NBTTag(((NbtByte)orig).byteValue());
        }
        if (TYPE == NbtShort.TYPE) {
            return new NBTTag(((NbtShort)orig).shortValue());
        }
        if (TYPE == NbtInt.TYPE) {
            return new NBTTag(((NbtInt)orig).intValue());
        }
        if (TYPE == NbtLong.TYPE) {
            return new NBTTag(((NbtLong)orig).longValue());
        }
        if (TYPE == NbtFloat.TYPE) {
            return new NBTTag(((NbtFloat)orig).floatValue());
        }
        if (TYPE == NbtDouble.TYPE) {
            return new NBTTag(((NbtDouble)orig).doubleValue());
        }
        if (TYPE == NbtByteArray.TYPE) {
            return new NBTTag(((NbtByteArray)orig).getByteArray());
        }
        if (TYPE == NbtString.TYPE) {
            return new NBTTag(((NbtString)orig).asString());
        }
        if (TYPE == NbtList.TYPE) {
            NBTList R = new NBTList();
            for (NbtElement i : (NbtList)orig) {
                R.append(convertNBTTag(i));
            }
            return new NBTTag(R);
        }
        if (TYPE == NbtCompound.TYPE) {
            final NbtCompound CVT = (NbtCompound)orig;
            NBTCompound R = new NBTCompound();
            for (String i : CVT.getKeys()) {
                R.set(new UTF8Sequence(i), convertNBTTag(CVT.get(i)));
            }
            return new NBTTag(R);
        }
        if (TYPE == NbtIntArray.TYPE) {
            return new NBTTag(((NbtIntArray)orig).getIntArray());
        }
        try {
            if (TYPE == NbtLongArray.TYPE) {
                return new NBTTag(((NbtLongArray)orig).getLongArray());
            }
        } catch (NoClassDefFoundError e) {
            return null; // before 17w18a(1.12)
        }
        return null;
    }

    public static NBTTagType
    convertNbtTagType(NbtType<? extends NbtElement> orig) {
        if (orig == NbtEnd.TYPE) {
            return NBTTagType.TAG_End;
        }
        if (orig == NbtByte.TYPE) {
            return NBTTagType.TAG_Byte;
        }
        if (orig == NbtShort.TYPE) {
            return NBTTagType.TAG_Short;
        }
        if (orig == NbtInt.TYPE) {
            return NBTTagType.TAG_Int;
        }
        if (orig == NbtLong.TYPE) {
            return NBTTagType.TAG_Long;
        }
        if (orig == NbtFloat.TYPE) {
            return NBTTagType.TAG_Float;
        }
        if (orig == NbtDouble.TYPE) {
            return NBTTagType.TAG_Double;
        }
        if (orig == NbtByteArray.TYPE) {
            return NBTTagType.TAG_Byte_Array;
        }
        if (orig == NbtString.TYPE) {
            return NBTTagType.TAG_String;
        }
        if (orig == NbtList.TYPE) {
            return NBTTagType.TAG_List;
        }
        if (orig == NbtCompound.TYPE) {
            return NBTTagType.TAG_Compound;
        }
        if (orig == NbtIntArray.TYPE) {
            return NBTTagType.TAG_Int_Array;
        }
        try {
            if (orig == NbtLongArray.TYPE) {
                return NBTTagType.TAG_Long_Array;
            }
        } catch (NoClassDefFoundError e) {
            return null; // before 17w18a(1.12)
        }
        return null;
    }

    public static NBTList convertNBTList(NbtList orig) {
        final NBTList RES = new NBTList();
        for (NbtElement i : orig) {
            RES.append(convertNBTTag(i));
        }
        return RES;
    }

    public static NBTCompound convertNBTCompound(NbtCompound orig) {
        final NBTCompound RES = new NBTCompound();
        for (String i : orig.getKeys()) {
            RES.set(new UTF8Sequence(i), convertNBTTag(orig.get(i)));
        }
        return RES;
    }
}