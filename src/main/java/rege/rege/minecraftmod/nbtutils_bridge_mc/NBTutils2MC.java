package rege.rege.minecraftmod.nbtutils_bridge_mc;

//import net.minecraft.command.argument.NbtPathArgumentType.NbtPath;
import net.minecraft.nbt.NbtByte;
import net.minecraft.nbt.NbtByteArray;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtDouble;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtEnd;
import net.minecraft.nbt.NbtFloat;
import net.minecraft.nbt.NbtInt;
import net.minecraft.nbt.NbtIntArray;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtLong;
import net.minecraft.nbt.NbtLongArray;
import net.minecraft.nbt.NbtShort;
import net.minecraft.nbt.NbtString;
import net.minecraft.nbt.NbtType;

import org.jetbrains.annotations.Nullable;

import rege.rege.nbtutils.NBTCompound;
import rege.rege.nbtutils.NBTList;
//import rege.rege.nbtutils.NBTPath;
import rege.rege.nbtutils.NBTTag;
import rege.rege.nbtutils.NBTTagType;
import rege.rege.utf8chr.UTF8Char;
import rege.rege.utf8chr.UTF8Sequence;

public class NBTutils2MC {
    private NBTutils2MC() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    public static NbtElement convertNbtElement(NBTTag orig)
    throws IllegalArgumentException {
        switch (orig.type) {
            case TAG_End: return NbtEnd.INSTANCE;
            case TAG_Byte: return NbtByte.of(orig.getAsByte());
            case TAG_Short: return NbtShort.of(orig.getAsShort());
            case TAG_Int: return NbtInt.of(orig.getAsInt());
            case TAG_Long: return NbtLong.of(orig.getAsLong());
            case TAG_Float: return NbtFloat.of(orig.getAsFloat());
            case TAG_Double: return NbtDouble.of(orig.getAsDouble());
            case TAG_Byte_Array:return new NbtByteArray(orig.getAsByteArray());
            case TAG_String: {
                for (UTF8Char i : orig.getAsString()) {
                    final long ORD = i.ord();
                    if (ORD > 0x10ffff || (ORD > 0xd7ff && ORD < 0xe000)) {
                        throw new IllegalArgumentException(
                            "Don't know how to convert codepoint " +
                            Long.toString(ORD) + " to UTF-16 form"
                        );
                    }
                }
                return NbtString.of(orig.getAsString().toString());
            }
            case TAG_List: {
                final NbtList RES = new NbtList();
                for (NBTTag i : orig.getAsList()) {
                    RES.add(convertNbtElement(i));
                }
                return RES;
            }
            case TAG_Compound: {
                final NbtCompound RES = new NbtCompound();
                for (UTF8Sequence i : orig.getAsCompound().names()) {
                    for (UTF8Char j : i) {
                        final long ORD = j.ord();
                        if (ORD > 0x10ffff || (ORD > 0xd7ff && ORD < 0xe000)) {
                            throw new IllegalArgumentException(
                                "Don't know how to convert codepoint " +
                                Long.toString(ORD) + " to UTF-16 form"
                            );
                        }
                    }
                    RES.put(i.toString(),
                            convertNbtElement(orig.getAsCompound().get(i)));
                }
                return RES;
            }
            case TAG_Int_Array: return new NbtIntArray(orig.getAsIntArray());
            case TAG_Long_Array: {
                try {
                    return new NbtLongArray(orig.getAsLongArray());
                } catch (NoClassDefFoundError e) {
                    return null; // before 17w18a(1.12)
                }
            }
            default: return null;
        }
    }

    @Nullable
    public static NbtType<? extends NbtElement> convertNbtType(NBTTagType orig){
        switch (orig) {
            case TAG_End: return NbtEnd.TYPE;
            case TAG_Byte: return NbtByte.TYPE;
            case TAG_Short: return NbtShort.TYPE;
            case TAG_Int: return NbtInt.TYPE;
            case TAG_Long: return NbtLong.TYPE;
            case TAG_Float: return NbtFloat.TYPE;
            case TAG_Double: return NbtDouble.TYPE;
            case TAG_Byte_Array: return NbtByteArray.TYPE;
            case TAG_String: return NbtString.TYPE;
            case TAG_List: return NbtList.TYPE;
            case TAG_Compound: return NbtCompound.TYPE;
            case TAG_Int_Array: return NbtIntArray.TYPE;
            case TAG_Long_Array: {
                try {
                    return NbtLongArray.TYPE;
                } catch (NoClassDefFoundError e) {
                    return null; // before 17w18a(1.12)
                }
            }
            default: return null;
        }
    }

    public static NbtList convertNbtList(NBTList orig)
    throws IllegalArgumentException {
        final NbtList RES = new NbtList();
        for (NBTTag i : orig) {
            RES.add(convertNbtElement(i));
        }
        return RES;
    }

    public static NbtCompound convertNbtCompound(NBTCompound orig)
    throws IllegalArgumentException {
        final NbtCompound RES = new NbtCompound();
        for (UTF8Sequence i : orig.names()) {
            for (UTF8Char j : i) {
                final long ORD = j.ord();
                if (ORD > 0x10ffff || (ORD > 0xd7ff && ORD < 0xe000)) {
                    throw new IllegalArgumentException(
                        "Don't know how to convert codepoint " +
                        Long.toString(ORD) + " to UTF-16 form"
                    );
                }
            }
            RES.put(i.toString(),
                    convertNbtElement(orig.get(i)));
        }
        return RES;
    }
}